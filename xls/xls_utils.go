package xls

import (
	"github.com/tealeg/xlsx"
	"gitlab.com/phuclam.nguyen/istockstaker/dto"
)

type ItemDAO struct {
	sheet *xlsx.Sheet
}

type ExtItem struct {
	dto.Item

	OrigRetailPrice string `json:"retailPriceWoVAT"`
}

func (itemDAO *ItemDAO) Init(xlsFile string) error {
	file, err := xlsx.OpenFile(xlsFile)
	if err != nil {
		return err
	} else {
		itemDAO.sheet = file.Sheets[0]
		return nil
	}
}

func (itemDAO *ItemDAO) ProcessItems(callback func([]dto.Item)) {
	buffer := make([]dto.Item, 0, 10)
	for _, row := range itemDAO.sheet.Rows {
		if len(buffer) < 10 {
			buffer = append(buffer, dto.Item{
				Code:        row.Cells[1].String(),
				VnName:      row.Cells[6].String(),
				RetailPrice: row.Cells[10].String(),
				ExtraPrice:  row.Cells[11].String(),
			})
		} else {
			callback(buffer)
			buffer = buffer[len(buffer):]
		}
	}

	if len(buffer) != 0 {
		callback(buffer)
	}
}

func (itemDAO *ItemDAO) ProcessOriginalItems(callback func([]ExtItem)) {
	for _, row := range itemDAO.sheet.Rows {
		callback([]ExtItem{
			{
				Item: dto.Item{

					Code:           row.Cells[1].String(),
					VnName:         row.Cells[6].String(),
					EngName:        row.Cells[5].String(),
					OrigPriceWoVAT: row.Cells[9].String(),
					OrigPrice:      row.Cells[11].String(),
					OrigVAT:        row.Cells[10].String(),
					RetailPrice:    row.Cells[14].String(),
					RetailVAT:      row.Cells[13].String(),
				},
				OrigRetailPrice: row.Cells[12].String(),
			},
		})
	}
}

func (itemDAO *ItemDAO) ProcessNewItems(callback func([]ExtItem)) {
	for _, row := range itemDAO.sheet.Rows {
		callback([]ExtItem{
			{
				Item: dto.Item{

					Code:           row.Cells[1].String(),
					VnName:         row.Cells[6].String(),
					EngName:        row.Cells[5].String(),
					RetailPrice:    row.Cells[9].String(),
				},
			},
		})
	}
}
