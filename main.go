package main

import (
	"context"
	"flag"
	"github.com/golang/glog"
	"gitlab.com/phuclam.nguyen/istockstaker-batch/firebase"
	"gitlab.com/phuclam.nguyen/istockstaker-batch/redis"
	"gitlab.com/phuclam.nguyen/istockstaker-batch/xls"
	"strings"
	"sync"
	"time"
)

func main() {
	var (
		path string
		data string
		redisHost string
	)

	flag.StringVar(&path, "file", "serviceAccountKey.json", "Specify a path to your serviceAccount.json")
	flag.StringVar(&data, "data", "", "Specify a path to your data excel file")
	flag.StringVar(&redisHost, "redis-host", "localhost", "Redis host")
	flag.Parse()

	if strings.Compare(path, "") == 0 || strings.Compare(data, "") == 0 {
		panic("Missing service account file or data file")
	}

	fbStore := firebase.FireBaseStore{}

	if err := fbStore.Init(path); err != nil {
		glog.Error("Unable to init FireBase")
		panic(err)
	}

	itemDAO := xls.ItemDAO{}
	if err := itemDAO.Init(data); err != nil {
		glog.Error("Unable to init XLS")
		panic(err)
	}

	var wg sync.WaitGroup
	pubsub, err := redis.NewItemPubSub(redisHost, &wg, func(item xls.ExtItem) error {
		if err := fbStore.UpdateItemPrice(context.TODO(), item.Item); err == firebase.ErrItemNotFound {
			glog.Warningf("Unable to find item %v, going to create a new item", item.Code)
			return fbStore.AddItem(context.TODO(), item)
		} else if err != nil {
			glog.Errorf("error processing item %s, error %v", item.Code, err)
			return err
		}

		glog.Infof("Successfully processed item %v", item.Code)
		return nil
	})

	if err != nil {
		glog.Error("Unable to create ItemPubSub")
		panic(err)
	}

	defer timeTrack(time.Now(), "Processing Items")
	itemDAO.ProcessNewItems(func(items []xls.ExtItem) {
		for _, item := range items {
			if glog.V(2) {
				glog.Infof("Publishing item %v", item)
			}

			wg.Add(1)
			pubsub.Publish(item)
		}
	})

	wg.Wait()
	if glog.V(2) {
		glog.Info("All items have been executed")
	}
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	glog.Infof("%s took %s", name, elapsed)
}
