package redis

import (
	"encoding/json"
	"github.com/golang/glog"
	"gitlab.com/phuclam.nguyen/istockstaker-batch/xls"
	"sync"
	"time"

	gredis "github.com/go-redis/redis"
)

const (
	channelName   = "items"
	maxBufferSize = 100
)

// NewItemPubSub returns an ItemPubSub that can publish and subscribe to a channel holding item data
func NewItemPubSub(redisHost string, wg *sync.WaitGroup, processor func(item xls.ExtItem) error) (*itemPubSub, error) {
	client := gredis.NewClusterClient(&gredis.ClusterOptions{
		Addrs: []string{redisHost},
	})

	pubsub := client.Subscribe(channelName)
	if _, err := pubsub.Receive(); err != nil {
		return nil, err
	}

	ch := pubsub.Channel()
	buffer := make(chan xls.ExtItem, maxBufferSize)

	go func() {
		defer func() {
			pubsub.Close()
			close(buffer)
		}()

		for msg := range ch {
			var item xls.ExtItem
			if err := json.Unmarshal([]byte(msg.Payload), &item); err != nil {
				if glog.V(2) {
					glog.Infof("Unable to unmarshal message %s", msg.Payload)
					continue
				}
			}
			glog.Infof("Spawning a goroutine to process %v", item)

			go func() {
				defer wg.Done()
				glog.Infof("Start to process item %v", item)
				if err := processor(item); err != nil {
					glog.Errorf("Item %v was not processed successfully, %v", item, err)
				}
			}()
		}
	}()

	go func() {
		for item := range buffer {
			bytes, err := json.Marshal(item)
			if err != nil {
				glog.Errorf("Unable to marshal item %v, error %v", item, err)
				continue
			}

			if err := client.Publish(channelName, bytes).Err(); err != nil {
				glog.Errorf("Unable to publish item %v, error %v", item, err)
				continue
			}
		}
	}()

	return &itemPubSub{
		grClient: client,
		ch:       buffer,
	}, nil
}

type itemPubSub struct {
	grClient *gredis.ClusterClient
	ch       chan xls.ExtItem
}

// Publish publishes data to the item channel
func (pubsub *itemPubSub) Publish(item xls.ExtItem) error {
	time.Sleep(time.Millisecond * 10)

	pubsub.ch <- item

	return nil
}
