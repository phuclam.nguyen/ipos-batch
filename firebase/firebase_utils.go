package firebase

import (
	"errors"
	"firebase.google.com/go"
	"firebase.google.com/go/db"
	"fmt"
	"github.com/golang/glog"
	"gitlab.com/phuclam.nguyen/istockstaker-batch/xls"
	"gitlab.com/phuclam.nguyen/istockstaker/dto"
	"golang.org/x/net/context"
	"google.golang.org/api/option"
)

type FireBaseStore struct {
	Database *db.Client
}

const (
	itemCollection = "items"
)

var (
	ErrItemNotFound = errors.New("unable to find item with code")
)

func (store *FireBaseStore) Init(serviceAcctFile string) error {
	conf := &firebase.Config{DatabaseURL: "https://inventorysearch-9682b.firebaseio.com/"}
	opt := option.WithCredentialsFile(serviceAcctFile)
	app, err := firebase.NewApp(context.Background(), conf, opt)
	if err != nil {
		return fmt.Errorf("error initializing app: %v", err)
	} else {
		db, err := app.Database(context.Background())

		if err != nil {
			return err
		} else {
			store.Database = db
			return nil
		}
	}
}

func (store *FireBaseStore) UpdateItemPrice(ctx context.Context, item dto.Item) error {
	items := store.Database.NewRef(itemCollection)
	queries, err := items.OrderByChild("Code").EqualTo(item.Code).GetOrdered(ctx)
	if err != nil {
		return err
	}

	for _, query := range queries {
		if glog.V(3) {
			glog.Infof("query key %v", query.Key())
		}
		items.Child(query.Key()).Update(
			ctx,
			map[string]interface{}{
				"retailPriceVAT": item.RetailPrice,
				"extraPrice":     item.ExtraPrice,
			})

		return nil
	}
	return ErrItemNotFound
}

// AddItem adds the given item to item collection
func (store *FireBaseStore) AddItem(ctx context.Context, item xls.ExtItem) error {
	items := store.Database.NewRef(itemCollection)

	ref, err := items.Push(ctx, item)
	if err == nil {
		if glog.V(2) {
			glog.Infof("Successfully added item %s, item ref: %s", item.Code, ref.Key)
		}
	}
	return err
}

type Item struct {
	Code           string
	VmName         string
	RetailPriceVAT string
	ExtraPrice     string
}

func InitFirebase() (*firebase.App, error) {
	return InitFirebaseWithAccountFile("../serviceAccountKey.json")
}

func InitFirebaseWithAccountFile(filename string) (*firebase.App, error) {
	conf := &firebase.Config{DatabaseURL: "https://inventorysearch-9682b.firebaseio.com/"}
	opt := option.WithCredentialsFile(filename)
	app, err := firebase.NewApp(context.Background(), conf, opt)
	if err != nil {
		return nil, fmt.Errorf("error initializing app: %v", err)
	}

	return app, nil
}

func Database(App *firebase.App) (*db.Client, error) {
	return App.Database(context.Background())
}
