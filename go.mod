module gitlab.com/phuclam.nguyen/istockstaker-batch

require (
	cloud.google.com/go v0.33.1 // indirect
	firebase.google.com/go v3.4.0+incompatible
	github.com/go-redis/redis v6.14.2+incompatible
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/googleapis/gax-go v2.0.2+incompatible // indirect
	github.com/tealeg/xlsx v1.0.3
	gitlab.com/phuclam.nguyen/istockstaker v0.0.0-20181126135616-2083b1144410
	golang.org/x/net v0.0.0-20181114220301-adae6a3d119a
	golang.org/x/oauth2 v0.0.0-20181120190819-8f65e3013eba // indirect
	google.golang.org/api v0.0.0-20181127174356-0a71a4356c3f
)
